import os
import menu
import settings
import requests


def is_valid(cmd) :
    return cmd in ['1','2']

   
def retry(func,session) :
    cmd = '0'
    while not(is_valid(cmd)) :
        cmd = input('1.try again.\n2.go back to the menu.\n')
    if cmd == '1' :
         func(session)
           
              
def register(session) :
    os.system('clear')
    name = input('Username : ').strip()
    pas = input('Password : ').strip()
    confpas = input('confirm password : ').strip()
    if confpas != pas :
       print('Your confirm password is incerroct!')
       retry(register,session)
    if ' ' in name or ' ' in pas :
       print("You can't use space in your username and password.")
       retry(register,session)
    response = session.post(settings.url['register'],data={'username' : name, 'password' : pas})

    if response.status_code == 200 :
       if str(response.content)[2:-1] == 'True' :#?
    	   print('Registration was successful!')
    	   input('press any key to go back to the menu.')
       else :
           print('This username has already been used.')
           retry(register,session)
    else :
       print('Registration failed!')
       retry(register,session)
    
              
def login(session):
    os.system('clear')
    name = input('Username : ')
    pas = input('Password : ')
    response = session.post(settings.url['login'],data={'username' : name, 'password' : pas})
    if response.status_code == 200 :
       r = str(response.content)[2:-1]
       print(r)
       if r == 'You are logged in.' :
    	   cmd = input('Enter "dummy" to go to dummy page or any key else to go back to the menu.')
    	   if cmd == 'dummy' :
    	      dummy()    
       else :
           retry(login,session)
    else :
       print('login failed!')
       retry(login,session)
    

def logout(session):
    os.system('clear')
    response = session.post(settings.url['logout'])
    if response.status_code == 200 :
       print(str(response.content)[2:-1])
       input('press any key to go back to the menu.')
    else :
       print('logout failed!')
       retry(logout,session)
 
 
def dummy():
    os.system('clear')
    response = requests.post(settings.url['dummy'])
    if response.status_code == 200 :
       print(str(response.content)[2:-1])
    else :
       print('connection failed!')
    cmd = input('Enter "dummy" to go to dummy page or any key else to go back to the menu.')
    if cmd == 'dummy' :
       dummy()
       
