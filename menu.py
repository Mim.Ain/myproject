import os
import network


def _show_menu(message):
    os.system('clear')
    if message:
        print(message)

    print('1. Login')
    print('2. Logout')
    print('3. Register')
    print('4. Exit')
    print()


def _is_valid(cmd):
    return cmd in ['1', '2', '3','4']


def _run(cmd,session):
    if cmd == '1':
        return network.login(session)

    if cmd == '2':
        return network.logout(session)

    if cmd == '3':
        return network.register(session)
    else :
    	exit()


def init(session):
    message = ''
    while True:
        _show_menu(message)

        cmd = input('Enter a number : ').strip()
        if _is_valid(cmd):
            message = _run(cmd,session)
